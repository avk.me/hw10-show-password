const passwordIcons = document.querySelectorAll('.icon-password');
const confirmPasswordIcons = document.querySelectorAll('.icon-confirm-password');
const inputPassword = document.querySelector('.password');
const inputConfirmPassword = document.querySelector('.confirm-password');
const passwordForm = document.querySelector(".password-form");
const warning = document.createElement('h2');

passwordForm.addEventListener('submit', function () {
    if (inputPassword.value === inputConfirmPassword.value && inputPassword.value !== "") {
        if (document.querySelector('.warning')) {
            document.querySelector('.warning').remove();
        }
        alert('You are welcome!')
    } else {
        if (!document.querySelector('.warning')) {
            warning.innerHTML = "Нужно ввести одинаковые значения";
            warning.classList.add('warning')
            warning.style.color = 'red';
            passwordForm.appendChild(warning);
        }
    }
    inputPassword.value = inputConfirmPassword.value = '';
});

passwordForm.addEventListener('click', function (event) {
    passwordIcons.forEach(icon => {
        if (event.target === icon) {
            inputPassword.toggleAttribute('type');
            if (inputPassword.hasAttribute('type')) {
                inputPassword.setAttribute('type', 'password')
            }
            passwordIcons.forEach(icon => icon.classList.toggle('hidden'));
        }
    })
    confirmPasswordIcons.forEach(icon => {
        if (event.target === icon) {
            inputConfirmPassword.toggleAttribute('type');
            if (inputConfirmPassword.hasAttribute('type')) {
                inputConfirmPassword.setAttribute('type', 'password')
            }
            confirmPasswordIcons.forEach(icon => icon.classList.toggle('hidden'));
        }
    })
})